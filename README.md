# TCP/IP PROTOCOLS AND NETWORKING

### THEORY AND HISTORY

The TCP/IP - transmission control protocol/internet protocol is a stack of protocols used to create a communication between computers over a network. In order to describe this communication the OSI model of seven layers was created. However this model was merely theoretical and the modern internet is based on the TCP/IP model.

OSI Model | TCP/IP model
--------- | -------------
physical layer | network access layer
data link |
network | internet
transport | transport
session |
presentation |
application | application

- The session+presentation+application layers in the OSI model are represented by the application layer in the TCP/IP suite. It's controllable by some programming language. The application (app itself, its representation  and the channel for communication).

- In the transport layer we create the transmission on two channels - TCP and UDP (different methods of sending the data). TCP and UDP have a list of ports, through which the data comes. For example,NTP uses port 123 on UDP side, HTTP uses port 80, DNS uses 53 on UDP side, etc. List of all known ports and more info are [here](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers)

- The internet layer holds the IP and the MAC addresses. When one sends data with IP protocol, it always does it while saving for itself source and destination of the data.

TCP/IP is a suite of protocols, such as NTP, HTTP, DNS, DHCP, SSH, SSL, etc.

- TCP takes data, divides it to smaller packages and gets them through specific ports. It is responsible on the software level to decide which protocol will be used and hence which port will be used.

- IP takes those packages and sends them to specific addresses (net or/and hardware).

### NET AND HARDWARE ADDRESSES

Some protocols work on TCP side (divide the data to packages and send it out through the ports), some work on the IP side (send data to either net address or to the hardware address). Net address is the IP of the NIC (network interface controller), hardware address is the MAC (media access control address).

#### MAC ADDRESSES

MAC is a unique ID of each card in the net, contains 6 pairs of numbers and is assigned by the manufacture. The first three pairs are for the manufacture, the last three pairs are for the specific card. The MAC addresses are transmitted through WiFi/cable to the recipient.

#### IP ADDRESSES
IP is the logical address of the NIC, is formed by a set of 32 bits, separated into four groups of 8 bits, represented in decimal form. [Here is the detailed explanation on how translate binary into decimal.](https://learing.lpi.org/en/learning-materials/102-500/109/109.1/109.1_01/)

For a example:

form |first octet | second octet | third octet | fourth octet
-----|-------|--------------|-------------|-------------
binary | 11000000.  | 10101000.    |00001010.    | 00010100
decimal | 192.       |168.          |10.          |20

0-255.0-255.0-255.0-255.

It limits the address space
